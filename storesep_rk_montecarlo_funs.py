import open3d as o3d 
import numpy as np
import datetime
import matplotlib.pyplot as plt
import copy
# from pandas.core.indexes.api import all_indexes_same
from scipy.integrate import RK45 
from scipy.interpolate import CubicSpline
from scipy.optimize import minimize
import pandas as pd
import pickle

# Import Geometry for nearest distance
tucanomesh_low_res = o3d.io.read_triangle_mesh('tucano_low_res.obj') #no wing/store
B3mesh_low_res = o3d.io.read_triangle_mesh('B3_low_res_3.obj')

#Import Geometry for Visualization
tucanomesh_high_res = o3d.io.read_triangle_mesh('tucano_08_18.obj')
tucanomesh_high_res.compute_vertex_normals() #to support shading
B3mesh = o3d.io.read_triangle_mesh('B3_full_scaled.obj')
B3mesh.compute_vertex_normals() #to support shading

#Import tail polar data, re = 1.3738e+6, naca 0012 airfoil
tail_polardata = pd.read_csv('B3_tail_polar.csv')
# Import Tunnel Data 166, define curves with cubic splines
dart_tunneldata = pd.read_csv('dart_tunnel_data_full_166.csv',delimiter='\t')

#Splines from Data
CL_alpha = CubicSpline(dart_tunneldata['ALPHAS_deg'],dart_tunneldata['CL'])
CD_alpha = CubicSpline(dart_tunneldata['ALPHAS_deg'],dart_tunneldata['CD'])
CM_alpha = CubicSpline(dart_tunneldata['ALPHAS_deg'],dart_tunneldata['CPMCG'])

CLt_alpha = CubicSpline(tail_polardata['alpha'],tail_polardata['CL'])
CLt_alpha_alpha =  lambda alpha: CLt_alpha(alpha,1) #1st derivative of CLt wrt alpha at given alpha

#Clip Splines - -14.7<aoa<20.4 deg
CL_alpha_clip = lambda alphas: CL_alpha(np.clip(alphas,a_min=min(dart_tunneldata['ALPHAS_deg']),a_max = max((dart_tunneldata['ALPHAS_deg']))))
CD_alpha_clip = lambda alphas: CD_alpha(np.clip(alphas,a_min=min(dart_tunneldata['ALPHAS_deg']),a_max = max((dart_tunneldata['ALPHAS_deg']))))
CM_alpha_clip = lambda alphas: CM_alpha(np.clip(alphas,a_min=min(dart_tunneldata['ALPHAS_deg']),a_max = max((dart_tunneldata['ALPHAS_deg']))))

CLt_alpha_clip = lambda alphas: CLt_alpha(np.clip(alphas,a_min=min(tail_polardata['alpha']),a_max = max((tail_polardata['alpha']))))
CLt_alpha_alpha_clip = lambda alphas: CLt_alpha_alpha(np.clip(alphas,a_min=min(tail_polardata['alpha']),a_max = max((tail_polardata['alpha']))))
# Constants from tunnel data, B3 geom
B3_length = 42/12 #ft
B3_radius = 0.5 #ft
B3_Sref = 0.186654782 #ft^2
B3_Lref = 0.4875 #ft (chord hopefully)
B3_mass = 25.6/32.2 #slugs
B3_CG_perc = (14.96/12)/B3_length #no dimension - wt data states cg @ 14.96 inches
B3_tail_quarter_chord = 29.92/12
B3_lt = B3_tail_quarter_chord-B3_length*B3_CG_perc #distance from cg to 1/4 chord of tail
B3_St = 32.4/(12*12) #horz tail area from vsp, ft^2
# B3_CG_perc = 0
g = 32.2 #ft/sec^2
tail_re = 1.3738e+6 #for reference later - not used in calc
#assume 2 constant width cylinders, each provide equal moment from cg
# nose_mass = B3_mass*(1-B3_CG_perc)
# tail_mass = B3_mass-nose_mass
# Iyy = ((0.25*nose_mass*B3_radius**2 + (1/3)*nose_mass*(B3_CG_perc*B3_length)**2)+ #forward Iyy
#         (0.25*tail_mass*B3_radius**2 + (1/3)*tail_mass*((1-B3_CG_perc)*B3_length)**2)) #aft Iyy 

#assume 2 point masses, x_cg away and L-x_cg away
# nose_mass = B3_mass*(1-B3_CG_perc)
# tail_mass = B3_mass-nose_mass
# Iyy = (nose_mass*(B3_CG_perc*B3_length)**2 + #forward Iyy
#       tail_mass*(B3_CG_perc*B3_length)**2) #aft Iyy 



Iyy = 2101.8/(12*12*g) #from MOI test

store = (11.010+4.76,-7.33,-2.2) #x,y,z coords of last point of store

class SimInputs:
    def __init__(self,
                 B3_initial_position, #tuple (x,y,z) position of nose at start
                 eject_speed, #ft/sec
                 v_freestream, #ft/sec
                 rho, #slug/ft^3
                 initial_aoa, #deg
                 q0, #initial pitch rate, deg/sec
                 roll_angle, #deg
                 y_acc_const, #ft/sec^2
                 z_acc_const, #ft/sec^2
                 n_time_steps, #integer number of time steps
                 end_time):
        self.B3_initial_position = B3_initial_position
        self.eject_speed = eject_speed
        self.v_freestream = v_freestream
        self.rho = rho
        self.initial_aoa = initial_aoa
        self.q0 = q0
        self.roll_angle = roll_angle
        self.y_acc_const = y_acc_const
        self.z_acc_const = z_acc_const
        self.n_time_steps = n_time_steps
        self.end_time = end_time
    # @property 
    # def CG_offset(self):
    #     return CG_offset(B3mesh_low_res,self.B3_initial_position).x

    @property 
    def translate_offset(self):
        #offset to use when translating with cg results
        global B3mesh_low_res_copy
        B3mesh_low_res_copy = copy.deepcopy(B3mesh_low_res)
        # B3mesh_copy.translate((B3_initial_position[0],B3_initial_position[2],-B3_initial_position[1]),relative=False) #move to initial point
        B3_verts = np.unique(np.asarray(B3mesh_low_res_copy.vertices),axis=0) #initial vertex locations
        nose_index = np.where(B3_verts==np.min(B3_verts,axis=0)[0])[0] #first index of minimum x point
        def nose_error(xyz):
            #given an offset from the initial CG, determine error of nose position
            global B3mesh_low_res_copy
            B3mesh_low_res_copy.translate((self.B3_CG_initial_position[0]+xyz[0],self.B3_CG_initial_position[2]+xyz[2],-self.B3_CG_initial_position[1]-xyz[1]),relative=False)
            B3_verts = np.unique(np.asarray(B3mesh_low_res_copy.vertices),axis=0)
            nose_vertex = list(B3_verts[nose_index,:][0])
            return np.sqrt((B3_initial_position[0]-nose_vertex[0])**2 + (B3_initial_position[1]+nose_vertex[2])**2 +(B3_initial_position[2]-nose_vertex[1])**2)
        return minimize(nose_error,[0,0,0],method='nelder-mead').x   #returns dx,dy,dz to give cg position and translate to correct location  

    @property
    def B3_CG_initial_position(self):
        #initial position of the nose
        # offset = self.coordinate_offset.x
        # return (self.B3_initial_position[0]+offset[0]+B3_length*B3_CG_perc,self.B3_initial_position[1]+offset[1],self.B3_initial_position[2]+offset[2])
        return (self.B3_initial_position[0]+B3_length*B3_CG_perc,self.B3_initial_position[1],self.B3_initial_position[2])
    


class SimResults:
    def __init__(self,
                 B3_time_results,
                 B3_results,
                 B3_translate_commands,
                 min_distance_results,
                 B3_closest_results,
                 tucano_closest_results,
                 initial_aoa,
                 roll_angle,
                 B3_CG_initial_position,
                 translate_offset):
        self.B3_time_results  = B3_time_results
        self.B3_results = B3_results
        self.B3_translate_commands = B3_translate_commands
        self.min_distance_results = min_distance_results
        self.B3_closest_results = B3_closest_results
        self.tucano_closest_results = tucano_closest_results
        self.initial_aoa  =initial_aoa
        self.roll_angle = roll_angle
        self.B3_CG_initial_position = B3_CG_initial_position
        self.translate_offset = translate_offset
    @property
    def struck(self):
        if np.any(self.min_distance_results<0.5):
            return True
        return False

# def CG_offset(B3_initial_position):
#     # B3_initial_position = (15.77,-7.1,-2.8) #x, y, z of nose in vsp coordinate convention - at last point of store
#     global B3mesh_copy
#     B3mesh_copy = copy.deepcopy(B3mesh)
#     # B3mesh_copy.translate((B3_initial_position[0],B3_initial_position[2],-B3_initial_position[1]),relative=False) #move to initial point
#     B3_verts = np.unique(np.asarray(B3mesh_low_res_copy.vertices),axis=0) #initial vertex locations
#     nose_index = np.where(B3_verts==np.min(B3_verts,axis=0)[0])[0] #first index of minimum x point
#     def nose_error(xyz):
#         #given cg offset, error of first nose point wrt wanted value
#         global B3mesh_low_res_copy
#         B3mesh_copy.translate((B3_initial_position[0]+xyz[0],B3_initial_position[2]+xyz[2],-B3_initial_position[1]-xyz[1]),relative=False)
#         B3_verts = np.unique(np.asarray(B3mesh_low_res_copy.vertices),axis=0)
#         nose_vertex = list(B3_verts[nose_index,:][0])
#         return np.sqrt((B3_initial_position[0]-nose_vertex[0])**2 + (B3_initial_position[1]+nose_vertex[2])**2 +(B3_initial_position[2]-nose_vertex[1])**2)
#     return minimize(nose_error,B3_initial_position,method='nelder-mead')


def closest_distance(B3mesh_low_res,tucanomesh_low_res,show=False):
    #given translated, rotated geometry, give closest point between them. Show=True to visually validate points
    #get vertices
    B3_verts = np.unique(np.asarray(B3mesh_low_res.vertices),axis=0)
    tucano_verts = np.unique(np.asarray(tucanomesh_low_res.vertices),axis=0) # from simplified mesh
    tucano_verts = tucano_verts[tucano_verts[:,0]>16] #behind wing
    tucano_verts = tucano_verts[tucano_verts[:,2]>0] #left side

    closest_dist = np.inf #preallocate - replaced on first iteration
    for i_tucano_vert in range(tucano_verts.shape[0]):
        for i_B3_vert in range(B3_verts.shape[0]):
            if np.sqrt(np.sum((tucano_verts[i_tucano_vert]-B3_verts[i_B3_vert])**2, axis=0))<closest_dist:
                closest_dist = np.sqrt(np.sum((tucano_verts[i_tucano_vert]-B3_verts[i_B3_vert])**2, axis=0))
                closest_B3 = B3_verts[i_B3_vert]
                closest_tucano = tucano_verts[i_tucano_vert]
    if show==True:
        #Show Cropped Tucano Geometry and B3, closest points
        fig,ax = plt.subplots()
        tucanocolor = 'b'
        B3color = 'k'
        closestcolor = 'r'
        ax.plot(tucano_verts[:,0],tucano_verts[:,2],'*',color=tucanocolor,label='Tucano')
        ax.plot(B3_verts[:,0],B3_verts[:,2],'*',color=B3color,label='B3')
        ax.plot(closest_tucano[0],closest_tucano[2],'*',color=closestcolor,label='Closest Dist')
        ax.legend()
        ax.plot(closest_B3[0],closest_B3[2],'*',color=closestcolor)
        ax.axis('equal')


        fig,ax = plt.subplots()
        ax.plot(tucano_verts[:,0],tucano_verts[:,1],'*',color=tucanocolor,label='Tucano')
        ax.plot(B3_verts[:,0],B3_verts[:,1],'*',color=B3color,label='B3')
        ax.plot(closest_tucano[0],closest_tucano[1],'*',color=closestcolor,label='Closest Dist')
        ax.legend()
        ax.plot(closest_B3[0],closest_B3[1],'*',color=closestcolor)
        ax.axis('equal')
        # plt.show()
    result = [closest_dist,closest_B3,closest_tucano]
    # return closest_dist
    return result



# def single_drop(B3_initial_position, #tuple (x,y,z)
#                eject_speed, #ft/sec
#                v_freestream, #ft/sec
#                rho, #slug/ft^3
#                initial_aoa, #deg
#                q0, #initial pitch rate, deg/sec
#                roll_angle, #deg
#                y_acc_const, #ft/sec^2
#                z_acc_const, #ft/sec^2
#                n_time_steps, #integer number of time steps
#                end_time): #sec
def single_drop(siminputs,const_pitch=False):
    B3_initial_position = siminputs.B3_initial_position #tuple (x,y,z) not used 
    B3_CG_initial_position = siminputs.B3_CG_initial_position
    eject_speed = siminputs.eject_speed #ft/sec
    v_freestream = siminputs.v_freestream #ft/sec
    rho = siminputs.rho #slug/ft^3
    initial_aoa = siminputs.initial_aoa #deg
    q0 = siminputs.q0 #initial pitch rate, deg/sec
    roll_angle = siminputs.roll_angle #deg
    y_acc_const = siminputs.y_acc_const #ft/sec^2
    z_acc_const = siminputs.z_acc_const #ft/sec^2
    n_time_steps = siminputs.n_time_steps #integer number of time steps
    end_time  = siminputs.end_time

    B3mesh_low_res_copy = copy.deepcopy(B3mesh_low_res)
    
    translate_offset = siminputs.translate_offset #translate(cg location + offset) translates the cg to the right place
    # Inputs
    # B3_initial_position = (15.77,-7.1,-2.8) #x, y, z of nose in vsp coordinate convention - at last point of store
    # q0 = 15 #deg/sec, initial pitch rate
    # eject_speed = 15 #ft/sec
    # v_freestream = 295.367 #ft/sec
    # initial_aoa = 3 #deg
    # roll_angle = 0 #deg
    # y_acc_const = 0 #ft/sec^2 left #TODO verify left
    # z_acc_const = 0 #ft/sec^2 up
    # rho = 0.00195684 #6500 ft
    # n_time_steps = 30
    # end_time = 2 #seconds

    # Constants
    # B3_length = 47/12 #ft
    # B3_radius = 0.5 #ft
    # B3_Sref = 0.186654782 #ft^2
    # B3_Lref = 0.4875 #ft (chord hopefully)
    # B3_mass = 25.6/32.2 #slugs
    # B3_CG_perc = (14.96/12)/B3_length #no dimension - wt data states cg @ 14.96 inches
    # B3_CG_initial_position = (B3_initial_position[0]+B3_length*B3_CG_perc,B3_initial_position[1],B3_initial_position[2])
    # g = 32.2 #ft/sec^2

    # Iyy = ((0.125*B3_mass*B3_radius**2 + (1/6)*B3_mass*(B3_CG_perc*B3_length)**2)+ #forward Iyy
    #       (0.125*B3_mass*B3_radius**2 + (1/6)*B3_mass*((1-B3_CG_perc)*B3_length)**2)) #aft Iyy
    # B3_CG_initial_position = (B3_initial_position[0]+B3_length*B3_CG_perc,B3_initial_position[1],B3_initial_position[2])
    
    if const_pitch==True:
        q0 = 0
    #Define Sim Initial Conditions
    y0 = [B3_CG_initial_position[0], # x
        B3_CG_initial_position[1], # y
        B3_CG_initial_position[2], # z 
        eject_speed, # x'
        0.0, # y'
        0.0, # z'
        initial_aoa, #degrees
        q0] #initial pitch rate, deg/sec

    #Define Sim Derivative Function
    def derivatives(t,y):
        # models as point - returns location of cg and aoa/moment 
        # t = time
        # y[0] = CG x
        # y[1] = CG y
        # y[2] = CG z
        # y[3] = CG x'
        # y[4] = CG y'
        # y[5] = CG z'
        # y[6] = aoa (degrees)
        # y[7] = q (pitch rate, degrees/sec)

        lift = 0.5*CL_alpha_clip(y[6])*rho*B3_Sref*(v_freestream-y[3])**2
        drag = 0.5*CD_alpha_clip(y[6])*rho*B3_Sref*(v_freestream-y[3])**2
        moment = 0.5*CM_alpha_clip(y[6])*rho*B3_Sref*B3_Lref*(v_freestream-y[3])**2
        delta_Lt = np.radians(y[7])*0.5*CLt_alpha_alpha_clip(y[6])*rho*B3_St*(v_freestream-y[3])**2 #change in tail lift, assumes no tail incidence
        delta_moment = -1*B3_lt*delta_Lt
        #tail pitch rate damping

        yp = np.zeros(8)
        yp[0] = y[3]
        yp[1] = y[4]
        yp[2] = y[5]
        yp[3] = drag/B3_mass
        yp[4] = y_acc_const-(lift*np.sin(np.radians(roll_angle))/B3_mass)
        yp[5] = z_acc_const+(lift*np.cos(np.radians(roll_angle))/B3_mass) - g
        yp[6] = y[7] #pitch rate, deg/sec
        if const_pitch==True:
            yp[7] = 0
        else:
            # yp[7] = np.degrees(moment/Iyy) #returns angular acceleration in degrees
            yp[7] = np.degrees((moment+delta_moment)/Iyy) #returns angular acceleration in degrees
        return yp

    #Create Sim 
    B3_sim = RK45(derivatives,t0=0,y0=y0,t_bound=end_time*1.5)
    #Preallocate Results
    dense_output_list = []
    dense_output_t_list = []
    #Run Sim
    while B3_sim.status=='running':
        B3_sim.step()
        #Save steps and dense output
        dense_output_list+=[B3_sim.dense_output()]
        dense_output_t_list+=[B3_sim.t_old]

        
    dense_output_t_list+=[B3_sim.t] #Add last time
    dense_output_t_list[0] = 0 #For inequalities in evaluation



    B3_time_results = np.linspace(dense_output_t_list[0],end_time,n_time_steps) #create time vector
    B3_results = np.zeros((len(B3_time_results),8)) #preallocate results array - 1 row per time step
    min_distance_results = np.zeros((len(B3_time_results)))
    B3_closest_results = np.zeros((len(B3_time_results),3))
    tucano_closest_results = np.zeros((len(B3_time_results),3))
    B3_translate_commands = np.zeros((len(B3_time_results),3))
    B3mesh_low_res_copy.translate((B3_CG_initial_position[0]+translate_offset[0],B3_CG_initial_position[2]+translate_offset[2],-B3_CG_initial_position[1]-translate_offset[2]),relative=False) #move to initial point

    for i_time,time in enumerate(B3_time_results):
        for i_dense_output, dense_output in enumerate(dense_output_list):
            if time>=dense_output_t_list[i_dense_output] and time<=dense_output_t_list[i_dense_output+1]:
                B3_results[i_time,:] = dense_output_list[i_dense_output](time)
                
            elif time>=dense_output_t_list[-1]:
                B3_results[i_time,:] = dense_output_list[-1](time)
            B3_translate_commands[i_time,:] = [B3_results[i_time,0]+translate_offset[0],B3_results[i_time,1]+translate_offset[1],B3_results[i_time,2]+translate_offset[2]]

        if i_time!=0:
            R = B3mesh_low_res_copy.get_rotation_matrix_from_zxy((np.radians(B3_results[i_time-1,6]),-np.radians(roll_angle),0)) #matrix to reverse orientation of last step
            B3mesh_low_res_copy.rotate(R,center=(B3_results[i_time-1,0],B3_results[i_time-1,2],-B3_results[i_time-1,1])) # center = (x, z, -y) unrotate about last simulation cg location

        B3mesh_low_res_copy.translate((B3_translate_commands[i_time,0],B3_translate_commands[i_time,2],-B3_translate_commands[i_time,1]),relative=False) # move to new cg location

        R = B3mesh_low_res_copy.get_rotation_matrix_from_yxz(((0,np.radians(roll_angle),-np.radians(B3_results[i_time,6])))) #matrix to rotate to current orientation
        B3mesh_low_res_copy.rotate(R,center=(B3_results[i_time,0],B3_results[i_time,2],-B3_results[i_time,1])) #rotate about cg
        distance_results = closest_distance(B3mesh_low_res_copy,tucanomesh_low_res)
        min_distance_results[i_time] = distance_results[0]
        B3_closest_results[i_time] = distance_results[1]
        tucano_closest_results[i_time] = distance_results[2]

    return SimResults(B3_time_results,
                 B3_results,
                 B3_translate_commands,
                 min_distance_results,
                 B3_closest_results,
                 tucano_closest_results,
                 initial_aoa,
                 roll_angle,
                 B3_CG_initial_position,
                 translate_offset)


def plot_sim_results(B3_time_results,B3_results,min_distance_results):
    #Plot All Results
    fig,axs = plt.subplots(nrows=6,ncols=1)
    ax = axs[0]
    ax.plot(B3_time_results,B3_results[:,0],label = 'X Location')
    ax.plot(B3_time_results,B3_results[:,1],label = 'Y Location')
    ax.plot(B3_time_results,B3_results[:,2],label = 'Z Location')
    ax.legend()
    ax = axs[1]
    ax.plot(B3_time_results,B3_results[:,3],label = 'X Speed')
    ax.plot(B3_time_results,B3_results[:,4],label = 'Y Speed')
    ax.plot(B3_time_results,B3_results[:,5],label = 'Z Speed')
    ax.legend()
    ax = axs[2]
    ax.plot(B3_time_results,B3_results[:,6],label = 'Pitch $(^\circ)$')
    ax.legend()
    ax = axs[3]
    ax.plot(B3_time_results,B3_results[:,7],label = 'Pitch Rate $(^\circ//sec)$')
    ax.legend()
    ax = axs[4]
    ax.plot(B3_time_results,CL_alpha_clip(B3_results[:,6]),label='$C_L$')
    ax.plot(B3_time_results,CD_alpha_clip(B3_results[:,6]),label='$C_D$')
    ax.plot(B3_time_results,CM_alpha_clip(B3_results[:,6]),label='$C_M$')
    ax.legend()
    ax = axs[5]
    ax.plot(B3_time_results,min_distance_results,label='Closest Distance to Fuse/Tail (min {} ft)'.format(min(min_distance_results)))
    ax.legend()

    ax.set(xlabel='Time(s)')
    plt.show()

def plot_multiple_sim_results(simresults_list,title=None):
    
    #Plot All Results
    fig,axs = plt.subplots(nrows=6,ncols=1)
    xcolor = 'tab:blue'
    ycolor = 'tab:orange'
    zcolor = 'tab:green'
    clcolor ='tab:purple'
    cdcolor = 'gold'
    cmcolor = 'lightseagreen'
    mindistcolor = 'tab:red'

    start=True
    for simresults in simresults_list:
        B3_time_results = simresults.B3_time_results
        B3_results = simresults.B3_results
        min_distance_results = simresults.min_distance_results
        ax = axs[0]
        ax.plot(B3_time_results,B3_results[:,0],label = 'X Location',color=xcolor)
        ax.plot(B3_time_results,B3_results[:,1],label = 'Y Location',color=ycolor)
        ax.plot(B3_time_results,B3_results[:,2],label = 'Z Location',color=zcolor)
        # ax.legend()
        ax = axs[1]
        ax.plot(B3_time_results,B3_results[:,3],label = 'X Speed',color=xcolor,linestyle='--')
        ax.plot(B3_time_results,B3_results[:,4],label = 'Y Speed',color=ycolor,linestyle='--')
        ax.plot(B3_time_results,B3_results[:,5],label = 'Z Speed',color=zcolor,linestyle='--')
        # ax.legend()
        ax = axs[2]
        ax.plot(B3_time_results,B3_results[:,6],label = 'Pitch $(^\circ)$',color=ycolor)
        # ax.legend()
        ax = axs[3]
        ax.plot(B3_time_results,B3_results[:,7],label = 'Pitch Rate $(^\circ//sec)$',color=ycolor,linestyle='--')
        # ax.legend()
        ax = axs[4]
        ax.plot(B3_time_results,CL_alpha_clip(B3_results[:,6]),label='$C_L$',color=clcolor)
        ax.plot(B3_time_results,CD_alpha_clip(B3_results[:,6]),label='$C_D$',color=cdcolor)
        ax.plot(B3_time_results,CM_alpha_clip(B3_results[:,6]),label='$C_M$',color=cmcolor)
        # ax.legend()
        ax = axs[5]
        
        # ax.plot(B3_time_results,B3_closest_results,label='Closest Distance to Fuse/Tail (min {} ft)'.format(min(B3_closest_results)),color=mindistcolor)
        ax.plot(B3_time_results,min_distance_results,label='Closest Distance to Fuse/Tail',color=mindistcolor)
        # ax.legend(loc='upper right')
        if start==True:
            for i_ax in range(6):
                axs[i_ax].legend(loc='upper right')
            start=False
    #last axis, last result
    ax.set(xlabel='Time(s)')
    if title is not None:
        fig.suptitle(title)
    plt.show()

def show_closest_line(simresults):
    #One sim
    B3_closest_results = simresults.B3_closest_results
    closest_B3_lines = [[i,i+1] for i in range(len(B3_closest_results)-1)]
    closest_B3_colors = [[1, 0, 0] for i in range(len(closest_B3_lines))]
    closest_B3_line_set = o3d.geometry.LineSet(
                                            points=o3d.utility.Vector3dVector(B3_closest_results),
                                            lines=o3d.utility.Vector2iVector(closest_B3_lines),
                                            )   
    closest_B3_line_set.colors = o3d.utility.Vector3dVector(closest_B3_colors)
    o3d.visualization.draw_geometries([tucanomesh_high_res,closest_B3_line_set])

def show_multiple_closest_lines(simresults_list):
    last_result_end = 0
    all_lines = []
    all_points = []
    for simresults in simresults_list:
        B3_closest_results = simresults.B3_closest_results
        all_lines+=[[i,i+1] for i in range(last_result_end,last_result_end+len(B3_closest_results)-1)]
        for B3_closest_point in B3_closest_results:
            all_points+=[list(B3_closest_point)]
        last_result_end=len(all_points)
    all_colors=[[1, 0, 0] for i in range(len(all_lines))]
    all_lines_set = o3d.geometry.LineSet(
                                         points=o3d.utility.Vector3dVector(all_points),
                                         lines=o3d.utility.Vector2iVector(all_lines),
                                         )
    all_lines_set.colors = o3d.utility.Vector3dVector(all_colors)
    o3d.visualization.draw_geometries([tucanomesh_high_res,all_lines_set])

def show_multiple_cg_lines(simresults_list):
    last_result_end = 0
    all_cg_lines = []
    all_cg_points = []
    for simresults in simresults_list:
        B3_CG_results = simresults.B3_results[:,0:3]
        all_cg_lines+=[[i,i+1] for i in range(last_result_end,last_result_end+len(B3_CG_results)-1)]
        for B3_cg_point in B3_CG_results:
            all_cg_points+=[[B3_cg_point[0],B3_cg_point[2],-B3_cg_point[1]]]
        last_result_end=len(all_cg_points)
    all_cg_colors=[[0, 0, 1] for i in range(len(all_cg_lines))]
    all_cg_lines_set = o3d.geometry.LineSet(
                                         points=o3d.utility.Vector3dVector(all_cg_points),
                                         lines=o3d.utility.Vector2iVector(all_cg_lines),
                                         )
    all_cg_lines_set.colors = o3d.utility.Vector3dVector(all_cg_colors)
    o3d.visualization.draw_geometries([tucanomesh_high_res,all_cg_lines_set])

def show_multiple_distance_lines(simresults_list):
    last_result_end = 0
    all_dist_lines = []
    all_dist_points = []
    for simresults in simresults_list:
        B3_closest_results = simresults.B3_closest_results
        tucano_closest_results = simresults.tucano_closest_results
        for i_closest_point in range(B3_closest_results.shape[0]):
            all_dist_points+=[list(B3_closest_results[i_closest_point,:])]
            all_dist_points+=[list(tucano_closest_results[i_closest_point,:])]
            all_dist_lines+=[[last_result_end+2*i_closest_point,last_result_end+2*i_closest_point+1]]
        last_result_end=len(all_dist_points)
    all_colors=[[0, 1, 0] for i in range(len(all_dist_lines))]
    all_dist_lines_set = o3d.geometry.LineSet(
                                         points=o3d.utility.Vector3dVector(all_dist_points),
                                         lines=o3d.utility.Vector2iVector(all_dist_lines),
                                         )
    all_dist_lines_set.colors = o3d.utility.Vector3dVector(all_colors)
    o3d.visualization.draw_geometries([tucanomesh_high_res,all_dist_lines_set])


def play_results(simresults_list,video_speed=1.0,camera_angle='left',show_closest=True,show_cg=True,show_dist=False,show_move_dist=True,frames_filename=None,record_runs=None,fov=5,move_on=False,zoom=None):
    global B3_time_ind, B3_max_time_ind, last_frame_showtime, initial_flag,record_frames,B3_copy,n_results,result_ind,move_dist_line,n_frame
    B3_copy= copy.deepcopy(B3mesh) #copy of the high res
    full_simresults_list = simresults_list #save this to make all displayed lines
    if record_runs is not None and len(simresults_list)>1:
        simresults_list = [simresults_list[record_run] for record_run in record_runs]
    #make video globals
    n_results = len(simresults_list)
    result_ind = 0
    n_frame = 0 #for recording images, image number
    B3_max_time_ind =  simresults_list[result_ind].B3_results.shape[0] -1 #highest index of results
    B3_time_ind = 0 #index of result to match displacement with time
    # B3_max_time_ind = B3_displacement_results.shape[0] -1 #highest index of results
    
    last_frame_showtime = datetime.datetime.now() #helps know when to play next frame
    initial_flag = True #flag to know when it's the first frame for setting camera angle
    
    front_camera_angle = np.array([-1,0,0]) #unit vectors to set camera front position for video
    left_camera_angle = np.array([0,0,1])
    iso_camera_angle = np.array([-1/np.sqrt(3),1/np.sqrt(3),1/np.sqrt(3)])
    back_iso_camera_angle = np.array([1/np.sqrt(3),1/np.sqrt(3),1/np.sqrt(3)])
    top_camera_angle = np.array([1,500,0])
    if camera_angle=='left':
        camera_angle = left_camera_angle
    elif camera_angle=='front':
        camera_angle = front_camera_angle
    elif camera_angle=='iso':
        camera_angle = iso_camera_angle
    elif camera_angle=='back iso':
        camera_angle=back_iso_camera_angle
    elif camera_angle=='top':
        camera_angle = top_camera_angle
    else:
        print('Using custom camera angle {}'.format(camera_angle))
    if frames_filename is not None:
        record_frames=True #flag to record (turns off after first cycle)
    else:
        record_frames = False #flag to record (turns off after first cycle)

    def B3_translate(vis):
        global B3_time_ind, B3_max_time_ind, last_frame_showtime,initial_flag,record_frames,B3_copy,result_ind,move_dist_line,n_frame

        B3_time_results = simresults_list[result_ind].B3_time_results
        B3_results = simresults_list[result_ind].B3_results
        B3_CG_initial_position = simresults_list[result_ind].B3_CG_initial_position
        initial_aoa = simresults_list[result_ind].initial_aoa
        roll_angle = simresults_list[result_ind].roll_angle
        B3_translate_commands = simresults_list[result_ind].B3_translate_commands
        translate_offset = simresults_list[result_ind].translate_offset
        B3_closest_results = simresults_list[result_ind].B3_closest_results
        tucano_closest_results = simresults_list[result_ind].tucano_closest_results

        
        # B3_closest_point
        if initial_flag==True:
            ctr = vis.get_view_control()
            ctr.set_front(camera_angle)
            ctr.set_lookat(np.array(store))
            if zoom is not None:
                ctr.set_zoom(zoom)
            if fov is not None:
                ctr.change_field_of_view(step=-90) 
                if fov>5:
                    ctr.change_field_of_view(step=fov-5)
            initial_flag = False
        current_time = datetime.datetime.now()
        time_since_last_frame = current_time-last_frame_showtime
        if B3_time_ind<=B3_max_time_ind:
            last_frame_time_step = datetime.timedelta(seconds=(B3_time_results[B3_time_ind]-B3_time_results[B3_time_ind-1])/video_speed)
        else:
            last_frame_time_step = datetime.timedelta(seconds=(B3_time_results[-1]-B3_time_results[-2])/video_speed) #same as second to last
            
        
        if time_since_last_frame>last_frame_time_step:
            #if it's been as long as it should have been since the last frame, update the frame
            if B3_time_ind>B3_max_time_ind:
                #if the end has already played
                B3_time_ind = 0
                #un-rotate
                R = B3_copy.get_rotation_matrix_from_zxy((np.radians(B3_results[-1,6]),-np.radians(roll_angle),0)) #matrix to reverse orientation y,-x,-+z
                B3_copy.rotate(R,center=(B3_results[-1,0],B3_results[-1,2],-B3_results[-1,1])) # center = (x, z, -y) rotate about simulation cg location
                vis.remove_geometry(move_dist_line,reset_bounding_box=False)
                if result_ind==n_results-1:
                    result_ind=0
                else:
                    result_ind+=1
                B3_time_results = simresults_list[result_ind].B3_time_results
                B3_results = simresults_list[result_ind].B3_results
                B3_CG_initial_position = simresults_list[result_ind].B3_CG_initial_position
                initial_aoa = simresults_list[result_ind].initial_aoa
                roll_angle = simresults_list[result_ind].roll_angle
                B3_max_time_ind =  simresults_list[result_ind].B3_results.shape[0] -1
                B3_copy.translate((B3_CG_initial_position[0]+translate_offset[0],B3_CG_initial_position[2]+translate_offset[2],-B3_CG_initial_position[1]-translate_offset[1]),relative=False) #move to next initial point
                R = B3_copy.get_rotation_matrix_from_yxz((0,np.radians(roll_angle),-np.radians(initial_aoa))) #matrix for initial orientation
                B3_copy.rotate(R,center=(B3_CG_initial_position[0],B3_CG_initial_position[2],-B3_CG_initial_position[1])) # center = (x, z, -y) rotate about initial cg location 
            else:  
                if B3_time_ind!=0:
                    if show_move_dist ==True:
                        vis.remove_geometry(move_dist_line,reset_bounding_box=False)
                    R = B3_copy.get_rotation_matrix_from_zxy((np.radians(B3_results[B3_time_ind-1,6]),-np.radians(roll_angle),0)) #matrix to reverse orientation of last step
                    B3_copy.rotate(R,center=(B3_results[B3_time_ind-1,0],B3_results[B3_time_ind-1,2],-B3_results[B3_time_ind-1,1])) # center = (x, z, -y) unrotate about last simulation cg location
                B3_copy.translate((B3_translate_commands[B3_time_ind,0],B3_translate_commands[B3_time_ind,2],-B3_translate_commands[B3_time_ind,1]),relative=False) # move to new cg location
                R = B3_copy.get_rotation_matrix_from_yxz(((0,np.radians(roll_angle),-np.radians(B3_results[B3_time_ind,6])))) #matrix to rotate to current orientation around cg
                B3_copy.rotate(R,center=(B3_results[B3_time_ind,0],B3_results[B3_time_ind,2],-B3_results[B3_time_ind,1]))
            # B3mesh.rotate(R,center=B3mesh.get_center())
            
            # print(B3_time_ind)
            if show_move_dist==True:
                move_dist_points = [list(B3_closest_results[B3_time_ind,:]),list(tucano_closest_results[B3_time_ind,:])]
                move_dist_line = [[0,1]]
                move_dist_line = o3d.geometry.LineSet(
                                            points=o3d.utility.Vector3dVector(move_dist_points),
                                            lines=o3d.utility.Vector2iVector(move_dist_line),
                                            )
                all_colors =  [[1,0.647,0]]                      
                move_dist_line.colors = o3d.utility.Vector3dVector(all_colors)
                vis.add_geometry(move_dist_line,reset_bounding_box = False)

            B3_time_ind+=1
            vis.update_geometry(B3_copy)
            vis.poll_events()
            vis.update_renderer()
            last_frame_showtime = current_time #record when this frame was shown
            if record_frames==True:
                n_frame+=1 #if recording multiple runs, keeps them from recording over each other
                image = vis.capture_screen_float_buffer(False)
                plt.imsave(('drop_frames\\'+frames_filename+'{:03d}.png').format(n_frame),np.asarray(image), dpi = 1)
                if B3_time_ind==len(B3_time_results)-1 and result_ind==n_results-1:
                    record_frames = False
                    if move_on==True:
                        vis.close()
            elif B3_time_ind==len(B3_time_results)-1 and result_ind==n_results-1 and move_on==True:
                vis.close()
        
    mesh_list = [tucanomesh_high_res,B3_copy]
    if show_closest==True:
        #Create lines to show on graph
        last_result_end = 0
        all_lines = []
        all_points = []
        for simresults in full_simresults_list:
            B3_closest_results = simresults.B3_closest_results
            all_lines+=[[i,i+1] for i in range(last_result_end,last_result_end+len(B3_closest_results)-1)]
            for B3_closest_point in B3_closest_results:
                all_points+=[list(B3_closest_point)]
            last_result_end=len(all_points)
        all_colors=[[1, 0, 0] for i in range(len(all_lines))]
        all_lines_set = o3d.geometry.LineSet(
                                            points=o3d.utility.Vector3dVector(all_points),
                                            lines=o3d.utility.Vector2iVector(all_lines),
                                            )
        all_lines_set.colors = o3d.utility.Vector3dVector(all_colors)
        mesh_list+=[all_lines_set]
        
    if show_cg==True:
        last_result_end = 0
        all_cg_lines = []
        all_cg_points = []
        for simresults in full_simresults_list:
            B3_CG_results = simresults.B3_results[:,0:3]
            all_cg_lines+=[[i,i+1] for i in range(last_result_end,last_result_end+len(B3_CG_results)-1)]
            for B3_cg_point in B3_CG_results:
                all_cg_points+=[[B3_cg_point[0],B3_cg_point[2],-B3_cg_point[1]]]
            last_result_end=len(all_cg_points)
        all_cg_colors=[[0, 0, 1] for i in range(len(all_cg_lines))]
        all_cg_lines_set = o3d.geometry.LineSet(
                                            points=o3d.utility.Vector3dVector(all_cg_points),
                                            lines=o3d.utility.Vector2iVector(all_cg_lines),
                                            )
        all_cg_lines_set.colors = o3d.utility.Vector3dVector(all_cg_colors)
        
        mesh_list+=[all_cg_lines_set]

    if show_dist==True:

        last_result_end = 0
        all_dist_lines = []
        all_dist_points = []
        for simresults in full_simresults_list:
            B3_closest_results = simresults.B3_closest_results
            tucano_closest_results = simresults.tucano_closest_results
            for i_closest_point in range(B3_closest_results.shape[0]):
                all_dist_points+=[list(B3_closest_results[i_closest_point,:])]
                all_dist_points+=[list(tucano_closest_results[i_closest_point,:])]
                all_dist_lines+=[[last_result_end+2*i_closest_point,last_result_end+2*i_closest_point+1]]
            last_result_end=len(all_dist_points)
        all_colors=[[0, 1, 0] for i in range(len(all_dist_lines))]
        all_dist_lines_set = o3d.geometry.LineSet(
                                            points=o3d.utility.Vector3dVector(all_dist_points),
                                            lines=o3d.utility.Vector2iVector(all_dist_lines),
                                            )
        all_dist_lines_set.colors = o3d.utility.Vector3dVector(all_colors)
        mesh_list+=[all_dist_lines_set]

    o3d.visualization.draw_geometries_with_animation_callback(mesh_list,B3_translate)




# RUN FUNCTIONS FOR TEST

# Inputs
# B3_initial_position = (15.77,-7.1,-2.8) #x, y, z of nose in vsp coordinate convention - at last point of store
# store = (11.010+4.76,-7.33,-2.2)#x, y, z of last point of store (created at top of file)
store_location = 3 #value 1-4 (1 presumably most constraining?)
                     # #
                   # 2 1 #  (view from back)
                   # 3 4 #
                     # #
store_location_angle = np.pi/4 +(store_location-1)*np.pi/2
store_d = 14/12 #this is what's in the vsp model, diameter of store pod in ft
store_location_r = store_d/4 #distance from center of pod to center of CLT
pod_location = (store[0],store[1]+store_location_r*np.cos(store_location_angle),store[2]+store_location_r*np.sin(store_location_angle)) #x,y,z coordinates of above
B3_initial_position = pod_location #initial point of nose - last x of store, at store location
v_freestream = 295.367 #ft/sec
rho = 0.00195684 #6500 ft
n_time_steps = 60
end_time = 6 #seconds
time_step = end_time/n_time_steps

# q0_sweep = [-15,0,30] #deg/sec, initial pitch rate
# eject_speed_sweep = [15,20,25] #ft/sec
# initial_aoa_sweep = [-20,-10,3,10,20] #deg
# roll_angle_sweep = [-90,-45,0,45,90] #deg
# y_acc_const_sweep = [0,16] #ft/sec^2 left 
# z_acc_const_sweep = [0,16] #ft/sec^2 up



# q0_sweep = [-15,0,15] #deg/sec, initial pitch rate
# eject_speed_sweep = [15,25] #ft/sec
# initial_aoa_sweep = [-15,0,15] #deg
# roll_angle_sweep = [-40,0,] #deg
# y_acc_const_sweep = [0,0.25*g] #ft/sec^2 (+ = right)
# z_acc_const_sweep = [0,0.25*g] #ft/sec^2  (+ = up)

q0_sweep = [0] #deg/sec, initial pitch rate
eject_speed_sweep = [15] #ft/sec
initial_aoa_sweep = [-20] #deg
roll_angle_sweep = [0] #deg
y_acc_const_sweep = [0] #ft/sec^2 (+ = right)
z_acc_const_sweep = [0] #ft/sec^2  (+ = up)




q0 = q0_sweep[0] #deg/sec, initial pitch rate
eject_speed = eject_speed_sweep[0] #ft/sec
initial_aoa = initial_aoa_sweep[0] #deg
roll_angle = roll_angle_sweep[0] #deg
y_acc_const = y_acc_const_sweep[0] #ft/sec^2 left 
z_acc_const = z_acc_const_sweep[0] #ft/sec^2 up




siminputs = SimInputs(B3_initial_position, #tuple (x,y,z)
                      eject_speed, #ft/sec
                      v_freestream, #ft/sec
                      rho, #slug/ft^3
                      initial_aoa, #deg
                      q0, #initial pitch rate, deg/sec
                      roll_angle, #deg
                      y_acc_const, #ft/sec^2
                      z_acc_const, #ft/sec^2
                      n_time_steps, #integer number of time steps
                      end_time)

simresults_list = []
simcount = 1
for q0 in q0_sweep:
    for eject_speed in eject_speed_sweep:
        for initial_aoa in initial_aoa_sweep:
            for roll_angle in roll_angle_sweep:
                for y_acc_const in y_acc_const_sweep:
                    for z_acc_const in z_acc_const_sweep:
                        print('3DOF Sim {0}/{1}'.format(simcount,len(q0_sweep)*len(eject_speed_sweep)*len(initial_aoa_sweep)*len(roll_angle_sweep)*len(y_acc_const_sweep)*len(z_acc_const_sweep)),end='')
                        siminputs.q0 = q0
                        siminputs.eject_speed = eject_speed
                        siminputs.initial_aoa = initial_aoa
                        siminputs.roll_angle = roll_angle
                        siminputs.y_acc_const = y_acc_const
                        siminputs.z_acc_const = z_acc_const
                        simresults_list+=[single_drop(siminputs)]
                        simcount+=1
                        if simresults_list[-1].struck:
                            print(' - Collision, {0}ft/sec eject, {1}deg initial AOA, {2}deg/sec q0, {3}deg roll, {4}ft/sec2 y, {5}ft/sec2 z'.format(eject_speed,initial_aoa,q0,roll_angle,y_acc_const,z_acc_const))
                        else:
                            print('')

record_runs = np.random.random_integers(0,simcount-1,2) #2 random runs
plot_multiple_sim_results(simresults_list,title='3DOF Results')
# play_results(simresults_list,video_speed=1,camera_angle=[-2,0.8,1],show_closest=True,show_cg=True,show_dist=False,show_move_dist=True,zoom=1/4) #fov=None uses render default, fov=5 is lowest fov (default for function)
play_results(simresults_list,video_speed=1,camera_angle=[-2,0.8,1],show_closest=True,show_cg=True,show_dist=False,show_move_dist=True,frames_filename='presentation_iso',record_runs=record_runs,move_on=True,zoom=1/4) #fov=None uses render default, fov=5 is lowest fov (default for function)
play_results(simresults_list,video_speed=1,camera_angle='back iso',show_closest=True,show_cg=True,show_dist=False,show_move_dist=True,frames_filename='presentation_back_iso',record_runs=record_runs,move_on=True,zoom=1/4) #fov=None uses render default, fov=5 is lowest fov (default for function)
play_results(simresults_list,video_speed=1,camera_angle='top',show_closest=True,show_cg=True,show_dist=False,show_move_dist=True,frames_filename='presentation_top',record_runs=record_runs,move_on=True,zoom=1/4) #fov=None uses render default, fov=5 is lowest fov (default for function)
play_results(simresults_list,video_speed=1,camera_angle='left',show_closest=True,show_cg=True,show_dist=False,show_move_dist=True,frames_filename='presentation_left',record_runs=record_runs,move_on=True,zoom=1/4) #fov=None uses render default, fov=5 is lowest fov (default for function)
play_results(simresults_list,video_speed=1,camera_angle='front',show_closest=True,show_cg=True,show_dist=False,show_move_dist=True,frames_filename='presentation_front',record_runs=record_runs,move_on=True,zoom=1/4) #fov=None uses render default, fov=5 is lowest fov (default for function)

const_pitch_simresults_list = []
simcount = 1
collisions_results_list = []
for eject_speed in eject_speed_sweep:
    for initial_aoa in initial_aoa_sweep:
        for roll_angle in roll_angle_sweep:
            for y_acc_const in y_acc_const_sweep:
                for z_acc_const in z_acc_const_sweep:
                    print('Const Pitch Sim {0}/{1}'.format(simcount,len(eject_speed_sweep)*len(initial_aoa_sweep)*len(roll_angle_sweep)*len(y_acc_const_sweep)*len(z_acc_const_sweep)),end='')
                    siminputs.eject_speed = eject_speed
                    siminputs.initial_aoa = initial_aoa
                    siminputs.roll_angle = roll_angle
                    siminputs.y_acc_const = y_acc_const
                    siminputs.z_acc_const = z_acc_const
                    const_pitch_simresults_list+=[single_drop(siminputs,const_pitch=True)]
                    simcount+=1
                    if const_pitch_simresults_list[-1].struck:
                        print(' - Collision, {0}ft/sec eject, {1}deg const AOA, {2}deg roll, {3}ft/sec2 y, {4}ft/sec2 z'.format(eject_speed,initial_aoa,roll_angle,y_acc_const,z_acc_const))
                        collisions_results_list+=[const_pitch_simresults_list[-1]]
                    else:
                        print('')
record_runs = np.random.random_integers(0,simcount-1,2) #2 random runs
play_results(const_pitch_simresults_list,video_speed=1,camera_angle=[-2,0.8,1],show_closest=True,show_cg=True,show_dist=False,show_move_dist=True,frames_filename='presentation_constpitch_iso',record_runs=record_runs,move_on=True,zoom=1/4) #fov=None uses render default, fov=5 is lowest fov (default for function)
play_results(const_pitch_simresults_list,video_speed=1,camera_angle='back iso',show_closest=True,show_cg=True,show_dist=False,show_move_dist=True,frames_filename='presentation_constpitch_back_iso',record_runs=record_runs,move_on=True,zoom=1/4) #fov=None uses render default, fov=5 is lowest fov (default for function)
play_results(const_pitch_simresults_list,video_speed=1,camera_angle='top',show_closest=True,show_cg=True,show_dist=False,show_move_dist=True,frames_filename='presentation_constpitch_top',record_runs=record_runs,move_on=True,zoom=1/4) #fov=None uses render default, fov=5 is lowest fov (default for function)
play_results(const_pitch_simresults_list,video_speed=1,camera_angle='left',show_closest=True,show_cg=True,show_dist=False,show_move_dist=True,frames_filename='presentation_constpitch_left',record_runs=record_runs,move_on=True,zoom=1/4) #fov=None uses render default, fov=5 is lowest fov (default for function)
play_results(const_pitch_simresults_list,video_speed=1,camera_angle='front',show_closest=True,show_cg=True,show_dist=False,show_move_dist=True,frames_filename='presentation_constpitch_front',record_runs=record_runs,move_on=True,zoom=1/4) #fov=None uses render default, fov=5 is lowest fov (default for function)


# plot_multiple_sim_results(simresults_list,title='3DOF Results')
# plot_multiple_sim_results(const_pitch_simresults_list,title='2DOF (Constant Pitch) Results')
# show_multiple_closest_lines(const_pitch_simresults_list)

# record_runs = [0,1]

# play_results(simresults_list,video_speed=1,camera_angle=[-2,0.8,1],show_closest=True,show_cg=True,show_dist=False,show_move_dist=True,zoom=1/4) 
# play_results(simresults_list,video_speed=1,camera_angle=[0,.4,0],show_closest=True,show_cg=True,show_dist=False,show_move_dist=True,zoom=1/4)

# play_results(simresults_list,video_speed=1,camera_angle='back iso',show_closest=True,show_cg=True,show_dist=False,show_move_dist=True,frames_filename='presentation_iso',record_runs=record_runs,move_on=True,fov=None) #fov=None uses render default, fov=5 is lowest fov (default for function)
# play_results(simresults_list,video_speed=1,camera_angle='left',show_closest=True,show_cg=True,show_dist=False,show_move_dist=True,frames_filename='presentation_left',record_runs=record_runs,move_on=True)
# play_results(simresults_list,video_speed=1,camera_angle='front',show_closest=True,show_cg=True,show_dist=False,show_move_dist=True,frames_filename='presentation_front',record_runs=record_runs,move_on=True)
# show_multiple_distance_lines(simresults_list)
# play_results(const_pitch_simresults_list,video_speed=1,camera_angle='left',show_closest=True,show_cg=True,show_dist=False,show_move_dist=True,frames_filename=None,fov=5)
# play_results(collisions_results_list,video_speed=1,camera_angle='left',show_closest=True,show_cg=True,show_dist=False,show_move_dist=True,frames_filename=None,fov=5)

# o3d.visualization.draw_geometries([B3mesh])
# o3d.visualization.draw_geometries([tucanomesh_high_res])
# show_multiple_cg_lines(simresults_list)

fig,ax = plt.subplots()
ax.plot(simresults_list[0].B3_time_results,simresults_list[0].min_distance_results,color='r',label='All Runs')
ax.legend()
for simresults in simresults_list:
    ax.plot(simresults.B3_time_results,simresults.min_distance_results,color='r')
ax.set(title='4DOF Proximity Results',xlabel='Time (s)',ylabel='Closest Distance (ft)',ylim=[0,15],xlim=[0,0.6])

fig,ax = plt.subplots()
ax.plot(const_pitch_simresults_list[0].B3_time_results,const_pitch_simresults_list[0].min_distance_results,color='r',label='All Runs')
ax.legend()
for simresults in const_pitch_simresults_list:
    ax.plot(simresults.B3_time_results,simresults.min_distance_results,color='r')
ax.set(title='Fixed Pitch Proximity Results',xlabel='Time (s)',ylabel='Closest Distance (ft)',ylim=[0,15],xlim=[0,0.6])

#super slow to show initial condition for screenshots
# play_results(simresults_list,video_speed=1/1000,camera_angle=[1,400,0],show_closest=False,show_cg=False,show_dist=False,show_move_dist=False,zoom=1/2)

print('Done')

#to save data 
with open('simresults.p','wb') as resultfile:
   pickle.dump(simresults_list, resultfile)
with open('constpitchsimresults.p','wb') as resultfile:
   pickle.dump(const_pitch_simresults_list, resultfile)

from storesep_rk_montecarlo_funs import play_results
#to open data 
with open('simresults.p','rb') as resultfile:
   simresults_list = pickle.load(resultfile)
record_runs = np.random.random_integers(0,len(simresults_list)-1,2) #2 random runs
play_results(simresults_list,video_speed=1,camera_angle=[-2,0.8,1],show_closest=True,show_cg=True,show_dist=False,show_move_dist=True,frames_filename='presentation_iso',record_runs=record_runs,move_on=True,zoom=1/4) #fov=None uses render default, fov=5 is lowest fov (default for function)
play_results(simresults_list,video_speed=1,camera_angle='back iso',show_closest=True,show_cg=True,show_dist=False,show_move_dist=True,frames_filename='presentation_back_iso',record_runs=record_runs,move_on=True,zoom=1/4) #fov=None uses render default, fov=5 is lowest fov (default for function)
play_results(simresults_list,video_speed=1,camera_angle='top',show_closest=True,show_cg=True,show_dist=False,show_move_dist=True,frames_filename='presentation_top',record_runs=record_runs,move_on=True,zoom=1/4) #fov=None uses render default, fov=5 is lowest fov (default for function)
play_results(simresults_list,video_speed=1,camera_angle='left',show_closest=True,show_cg=True,show_dist=False,show_move_dist=True,frames_filename='presentation_left',record_runs=record_runs,move_on=True,zoom=1/4) #fov=None uses render default, fov=5 is lowest fov (default for function)
play_results(simresults_list,video_speed=1,camera_angle='front',show_closest=True,show_cg=True,show_dist=False,show_move_dist=True,frames_filename='presentation_front',record_runs=record_runs,move_on=True,zoom=1/4) #fov=None uses render default, fov=5 is lowest fov (default for function)



with open('constpitchsimresults.p','rb') as resultfile:
   const_pitch_simresults_list = pickle.load(resultfile)

record_runs = np.random.random_integers(0,len(const_pitch_simresults_list)-1,2) #2 random runs
play_results(const_pitch_simresults_list,video_speed=1,camera_angle=[-2,0.8,1],show_closest=True,show_cg=True,show_dist=False,show_move_dist=True,frames_filename='presentation_constpitch_iso',record_runs=record_runs,move_on=True,zoom=1/4) #fov=None uses render default, fov=5 is lowest fov (default for function)
play_results(const_pitch_simresults_list,video_speed=1,camera_angle='back iso',show_closest=True,show_cg=True,show_dist=False,show_move_dist=True,frames_filename='presentation_constpitch_back_iso',record_runs=record_runs,move_on=True,zoom=1/4) #fov=None uses render default, fov=5 is lowest fov (default for function)
play_results(const_pitch_simresults_list,video_speed=1,camera_angle='top',show_closest=True,show_cg=True,show_dist=False,show_move_dist=True,frames_filename='presentation_constpitch_top',record_runs=record_runs,move_on=True,zoom=1/4) #fov=None uses render default, fov=5 is lowest fov (default for function)
play_results(const_pitch_simresults_list,video_speed=1,camera_angle='left',show_closest=True,show_cg=True,show_dist=False,show_move_dist=True,frames_filename='presentation_constpitch_left',record_runs=record_runs,move_on=True,zoom=1/4) #fov=None uses render default, fov=5 is lowest fov (default for function)
play_results(const_pitch_simresults_list,video_speed=1,camera_angle='front',show_closest=True,show_cg=True,show_dist=False,show_move_dist=True,frames_filename='presentation_constpitch_front',record_runs=record_runs,move_on=True,zoom=1/4) #fov=None uses render default, fov=5 is lowest fov (default for function)
